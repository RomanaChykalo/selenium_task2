import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GmailTest {

    private WebDriver driver;
    private final static String CITE = "https://gmail.com";
    private final static String USER_MAIL = "0937597167r";
    private final static String RECEIVER_MAIL = "romawka92.92@mail.ru";
    private final static String LETTER_SUBJECT = "Hello, i am bot";
    private final static String PASS = "140992musepa";
    private static Logger logger = LogManager.getLogger(GmailTest.class);

    static {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeMethod
    public void invokeBrowser() {
        driver = DriverManager.getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.get(CITE);
    }

    @Test
    public void sendLetter() {
        WebElement signInBtn = driver.findElement(By.cssSelector("ul li:nth-child(2) a"));
        signInBtn.click();
        WebElement userEmailLine = driver.findElement(By.cssSelector("input#identifierId"));
        userEmailLine.sendKeys(USER_MAIL);
        driver.findElement(By.id("identifierNext")).click();
        WebElement passwordLine = driver.findElement(By.xpath("//div[@id='password']//input[@name='password']"));
        passwordLine.sendKeys(PASS);
        driver.findElement(By.id("passwordNext")).click();
        logger.info("You have already log in! Write a letter");

        WebElement writeLetterBtn = driver.findElement(By.xpath("//div[@role='button' and ancestor::*[@class='z0']]"));
        writeLetterBtn.click();
        WebElement emailReceiver = driver.findElement(By.name("to"));
        emailReceiver.sendKeys(RECEIVER_MAIL);
        WebElement letterTopicLine = driver.findElement(By.xpath("//input[@name ='subjectbox' and following-sibling::*[@class='bzm']]"));
        letterTopicLine.sendKeys(LETTER_SUBJECT);
        WebElement sendLetterBtn = driver.findElement(By.cssSelector("div.dC [role='button']:nth-of-type(1)"));
        sendLetterBtn.click();
        logger.info("You sent a letter! Check you output letter's list");

        WebElement gmailSearchLine = driver.findElement(By.cssSelector("input.gb_He"));
        gmailSearchLine.sendKeys("in:sent ");
        WebElement gmailSearchBtn = driver.findElement(By.cssSelector("#aso_search_form_anchor > button.gb_Qe.gb_Re"));
        gmailSearchBtn.click();
        WebElement lastLetter = driver.findElement(By.xpath("/html/body[@class='aAU']/div[7]/div[@class='nH']/div[@class='nH']/div[2]/div[@class='no']/div[2]//div[@class='Tm aeJ']/div[@class='aeF']//div[@role='main']/div[3]//table[@class='F cf zt']/tbody/tr[1]"));
        lastLetter.click();
        String lastLetterReceiverEmail = driver.findElement(By.className("g2")).getAttribute("data-hovercard-id");
        logger.info("Last letter receiver is: "+lastLetterReceiverEmail);
        Assert.assertEquals(lastLetterReceiverEmail.trim(),RECEIVER_MAIL);
    }

    @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
